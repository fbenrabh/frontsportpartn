import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { EvenListComponent } from './even-list/even-list.component';
import { SingleEvenComponent } from './even-list/single-even/single-even.component';
import { EvenFormComponent } from './even-list/even-form/even-form.component';
import { HeaderComponent } from './header/header.component';
import { MeteoComponent } from './meteo/meteo.component';
import { AccueilComponent } from './accueil/accueil.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { EvenService } from './services/even.service';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCarouselModule } from '@ngmodule/material-carousel';
import { AuthGuardService } from './services/auth-guard.service';

import { AngularFireModule } from '@angular/fire';
import { AutourComponent } from './autour/autour.component';
import { UpdateEvenComponent } from './even-list/update-even/update-even.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ExternalApisService } from './services/external-apis.service';
import { TrustUrlPipe } from './trust-url.pipe';
import { ProfileComponent } from './profile/profile.component';
import { UserService } from './services/user.service';
import { FooterComponent } from './footer/footer.component';
import { ProfileFormComponent } from './profile/profile-form/profile-form.component';

const appRoutes: Routes = [
  
  { path: 'auth/signup', component: SignupComponent },
  { path: 'auth/signin', component: SigninComponent },
  { path: 'accueil', component: AccueilComponent },
  { path: 'meteo', component: MeteoComponent },
  { path: 'autour', component: AutourComponent },
  { path: 'even',  canActivate: [AuthGuardService],component: EvenListComponent },
  { path: 'even/new',canActivate: [AuthGuardService],component: EvenFormComponent },
  { path: 'even/view/:id', canActivate: [AuthGuardService],component: SingleEvenComponent },
  { path: 'even/update/:id', component: UpdateEvenComponent },
  { path: 'profile/view/:id', canActivate: [AuthGuardService], component: ProfileComponent },
  { path: 'profile/edit', component: ProfileFormComponent },
  { path: '', redirectTo: 'accueil', pathMatch: 'full' },
  { path: '**', redirectTo: 'accueil' }

];


@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    EvenListComponent,
    SingleEvenComponent,
    EvenFormComponent,
    HeaderComponent,
    MeteoComponent,
    AccueilComponent,
    AutourComponent,
    UpdateEvenComponent,
    TrustUrlPipe,
    ProfileComponent,
    FooterComponent,
    ProfileFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    MatCarouselModule.forRoot(),
    AngularFireModule.initializeApp(yourFirebaseConfig),
    FlexLayoutModule
  ],
  providers: [
    AuthService,
    EvenService,
    AuthGuardService,
    ExternalApisService,
    UserService
    ],
   
  bootstrap: [AppComponent]
})
export class AppModule { }

function yourFirebaseConfig(yourFirebaseConfig: any): any[] | import("@angular/core").Type<any> | import("@angular/core").ModuleWithProviders<{}> {
  throw new Error('Function not implemented.');
}
  
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Even } from '../../models/even.model';
import { EvenService } from '../../services/even.service';
import { Router } from '@angular/router';
import  firebase from 'firebase';

@Component({
  selector: 'app-even-form',
  templateUrl: './even-form.component.html',
  styleUrls: ['./even-form.component.scss']
})
export class EvenFormComponent implements OnInit {


  evenForm!: FormGroup;
  fileIsUploading = false;
  fileUrl!: string;
  fileUploaded = false;
  sports!:string[]; 

  constructor(private formBuilder: FormBuilder, private evensService: EvenService,
              private router: Router) { }
              
  ngOnInit() {
    this.sports = this.getSports();
    this.initForm();
  }


  initForm() {
    this.evenForm = this.formBuilder.group({
      title: ['',Validators.required],
      eventAddress: ['', Validators.required],
      eventDate: ['', Validators.required],
      city: ['', Validators.required],
      postalCode: ['', Validators.required],
      description: ' ',
      selectSport: ['', Validators.required]
    });
  }

//ajouter ? pour possible null
  onSaveEvent() {
    const title = this.evenForm.get('title')?.value;
    const eventDate = this.evenForm.get('eventDate')?.value;
    const eventAddress = this.evenForm.get('eventAddress')?.value;
    const city= this.evenForm.get('city')?.value;
    const postalCode= this.evenForm.get('postalCode')?.value;
    const description = this.evenForm.get('description')?.value;
    const selectSport = this.evenForm.get('selectSport')?.value;
    
    const newEven = new Even();
    newEven.description = description;  
    newEven.eventDate = eventDate;
    newEven.city=city;
    newEven.title=title;
    newEven.eventAddress=eventAddress; 
    newEven.postalCode=postalCode;
    newEven.sport = selectSport;
    
   if(this.evensService.createNewEven(newEven)){
       this.router.navigate(['/even']);
   }
}
  onUploadFile(file: File) {
    this.fileIsUploading = true;
    this.evensService.uploadFile(file).then(
      (url: string ) => {
        this.fileUrl = url;
        this.fileIsUploading = false;
        this.fileUploaded = true;
      }
    );
}
detectFiles(event: any) {
  this.onUploadFile(event.target.files[0]);
}


getSports(){
  return this.evensService.getSportsList();
}


}

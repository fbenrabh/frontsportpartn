import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvenFormComponent } from './even-form.component';

describe('EvenFormComponent', () => {
  let component: EvenFormComponent;
  let fixture: ComponentFixture<EvenFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EvenFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvenFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

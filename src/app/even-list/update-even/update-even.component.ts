import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EvenService } from 'src/app/services/even.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Even } from '../../models/even.model';
import  firebase from 'firebase';

@Component({
  selector: 'app-update-even',
  templateUrl: './update-even.component.html',
  styleUrls: ['./update-even.component.scss']
})
export class UpdateEvenComponent implements OnInit {

  
  evenForm!: FormGroup;
  fileIsUploading = false;
  fileUrl!: string;
  fileUploaded = false;

  constructor(private route: ActivatedRoute,private formBuilder: FormBuilder, private evensService: EvenService,
              private router: Router) { }
              
  ngOnInit() {
    this.initForm();
  }

    initForm() {
      this.evenForm = this.formBuilder.group({
        sport: ['', Validators.required],
        lieu: ['', Validators.required],
        date: ['', Validators.required],
        heure: ['', Validators.required],
        description: ''
      });
    }

    onUpdateEven() {
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
      const sport = this.evenForm.get('sport')?.value;
      const lieu = this.evenForm.get('lieu')?.value;
      const date = this.evenForm.get('date')?.value;
      const heure = this.evenForm.get('heure')?.value;
  
      const description = this.evenForm.get('description')?.value;
      
      const newEven = new Even();
      newEven.description = description;  
      newEven.heure = heure;  
  
      if(this.fileUrl && this.fileUrl !== '') {
        newEven.photo = this.fileUrl;
      }


      const id = this.route.snapshot.params['id'];

      this.evensService.updateEven(id,newEven);


      this.router.navigate(['/even']);
  
    }
  });
  }


  onUploadFile(file: File) {
    this.fileIsUploading = true;
    this.evensService.uploadFile(file).then(
      (url: string ) => {
        this.fileUrl = url;
        this.fileIsUploading = false;
        this.fileUploaded = true;
      }
    );
}
detectFiles(event: any) {
  this.onUploadFile(event.target.files[0]);
}


}

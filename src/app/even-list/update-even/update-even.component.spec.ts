import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateEvenComponent } from './update-even.component';

describe('UpdateEvenComponent', () => {
  let component: UpdateEvenComponent;
  let fixture: ComponentFixture<UpdateEvenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateEvenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateEvenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

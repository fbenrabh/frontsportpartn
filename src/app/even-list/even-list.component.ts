import { Component,OnChanges,OnDestroy, OnInit } from '@angular/core';
import {Even} from '../models/even.model';
import {Subscription} from 'rxjs/index';
import {EvenService} from '../services/even.service';
import {Router} from '@angular/router';
import  firebase from 'firebase';
import { User } from '../models/user.model';
import { isGeneratedFile } from '@angular/compiler/src/aot/util';
import { SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-even-list',
  templateUrl: './even-list.component.html',
  styleUrls: ['./even-list.component.scss']
})

export class EvenListComponent implements OnInit, OnDestroy{
   
  userActuel="";

  
  //evensSubscription!: Subscription;
  //event!:Even;
  eventsList!:Even[];
  constructor(public evensService: EvenService, private router: Router) { 
   
  }



  ngOnInit() {
    //this.eventsList = this.evensService.eventsList;
    
    this.eventsList =  this.evensService.getEvents();
    
    
    //this.router.navigate(['/even']);
   
  }

  onAddEvent(){
    this.router.navigate(['/even/new']);
  }







  onNewEven() {
    this.router.navigate(['/even', 'new']);
  }

  onUpdateEven(id: number) {
    this.router.navigate(['/even', 'update', id]);
  }

  onDeleteEven(even: Even) {
    //this.evensService.removeEven(even);
  }

  onViewEven(id: number) {
    this.router.navigate(['/even', 'view', id]);
  }

  ngOnDestroy(): void {
    //this.evensSubscription.unsubscribe();
  }


}

import { getLocaleDateTimeFormat } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Even } from '../../models/even.model';
import { EvenService } from '../../services/even.service';
import {ExternalApisService} from '../../services/external-apis.service';
import {  SafeResourceUrl,DomSanitizer } from '@angular/platform-browser';
import {TrustUrlPipe} from '../../trust-url.pipe';
import { GeoEncode } from 'src/app/models/geo-encode.model';

@Component({
  selector: 'app-single-even',
  templateUrl: './single-even.component.html',
  styleUrls: ['./single-even.component.scss', "../../../assets/vendors/bootstrap/css/bootstrap.css",
  "../../../assets/vendors/font-awesome/css/fontawesome-all.min.css","../../../assets/vendors/magnific-popup/magnific-popup.css",
  "../../../assets/css/styles.css"]
})
export class SingleEvenComponent implements OnInit {

  safeUrl? : SafeResourceUrl;
  event!: Even ;
  geoEncode!:GeoEncode;
  url!:string;
  currUsr!:any;
  //isParticipate!:boolean;

  constructor(private route: ActivatedRoute, private evensService: EvenService,private sanitizer :DomSanitizer,
              private router: Router, private extApiService: ExternalApisService) {

              }

  ngOnInit() {
    //this.url = "https://www.openstreetmap.org/export/embed.html?bbox=1.8518856167793276%2C50.94634874487112%2C1.854396164417267%2C50.94752829403982&amp;layer=mapnik&amp;marker=50.946938523196806%2C1.8531408905982971";
    this.event = new Even();

    if(localStorage.getItem("currentUser")!==null){
      this.currUsr = localStorage.getItem("currentUser");
    }
   
  
    this.geoEncode = new GeoEncode();
    const id:string = this.route.snapshot.params['id'];
    this.event = this.evensService.getEvent(id);
    //this.geoEncode = this.extApiService.addressGeocoding("50 ferdinand","calais","62100");
    //this.url = this.getUrl(this.geoEncode);
    //console.log("aaa "+this.geoEncode);
    //this.url = "https://www.openstreetmap.org/export/embed.html?bbox="+this.geoEncode.bbox[2]+"%2C"+this.geoEncode.bbox[0]+"%2C"+this.geoEncode.bbox[3]+"%2C"+this.geoEncode.bbox[1]+"&layer=mapnik&marker="+this.geoEncode.lat+"%2C"+this.geoEncode.lon;

    //this.url = this.extApiService.getMapUrl("50 ferdinand","calais","62100");
 
  }

  getUrl(geo:GeoEncode):string{
    return this.extApiService.getMapUrl(geo);
    return "#";
  }


  
  onBack() {
    this.router.navigate(['/even']);
  }

  deleteEvent(){
    
    this.evensService.deleteEvent(this.event);
    this.router.navigate(['/even']);
    //window.location.reload();

    
  }

  participate(){
  
    this.evensService.participeToEvent(this.event);
    //this.router.navigate(['/even/'+this.event.id]);
  }
  removeParticipe(){
    this.evensService.removeParticipation(this.event);
    //this.router.navigate(['/even/'+this.event.id]);
  }




}

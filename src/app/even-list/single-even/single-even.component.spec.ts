import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleEvenComponent } from './single-even.component';

describe('SingleEvenComponent', () => {
  let component: SingleEvenComponent;
  let fixture: ComponentFixture<SingleEvenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleEvenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleEvenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

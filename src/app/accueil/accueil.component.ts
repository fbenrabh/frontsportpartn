import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatCarousel, MatCarouselComponent } from '@ngmodule/material-carousel';
@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AccueilComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

   
   // Slider Images
   slides = [{'image': '../assets/images/foot1.jpg'},
   {'image': '../assets/images/foot2.jpg'},
   {'image': '../assets/images/basket1.jpg'},
   {'image': '../assets/images/foot3.jpg'},
   {'image': '../assets/images/foot4.jpg'},
   {'image': '../assets/images/foot5.jpg'},
   {'image': '../assets/images/basket2.jpg'},
   {'image': '../assets/images/foot6.jpg'},
   {'image': '../assets/images/Boxe.jpg'}]

    
    

}

import { Time } from "@angular/common";
import { GeoEncode } from "./geo-encode.model";
import { User } from "./user.model";

export class Even {
    id!:string;
    title!:string;
    photo!: string;
    description!: string;
    heure!: Time;
    sport!: string;
    creationDate!:Date;
    eventDate!:Date;
    eventAddress!:string;
    city!:string;
    postalCode!:string;
    author!: User;
    participants!:User[];
    geoEncode!: GeoEncode;
    isParticipate!:boolean;
    
    constructor();
    constructor( public lieu?: string, public proprietaire?: string,  public creationdate?: Date) {
    }

   

  }
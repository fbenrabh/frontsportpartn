import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss',"../../assets/vendors/bootstrap/css/bootstrap.css",
  "../../assets/vendors/font-awesome/css/fontawesome-all.min.css","../../assets/vendors/magnific-popup/magnific-popup.css",
  "../../assets/css/styles.css"]
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import  firebase from 'firebase';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss',
  "../../assets/vendors/bootstrap/css/bootstrap.css",
  "../../assets/vendors/font-awesome/css/fontawesome-all.min.css","../../assets/vendors/magnific-popup/magnific-popup.css",
  "../../assets/css/styles.css"]
})



export class HeaderComponent implements OnInit {
  navbarOpen = false;
  uid?:string;
  fullname?:string;
  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  

  isAuth: boolean = false;
  user!:User;
  id!:any;

  constructor(private authService: AuthService,private router:Router,private userService:UserService ,
     private route:ActivatedRoute) { }

  ngOnInit() {

    this.id = (localStorage.getItem("currentUser")!==null)? localStorage.getItem("currentUser") : "";
    this.user = this.userService.getUser(this.id);
  }

  onSignOut() {
    this.authService.signOutUser();
    this.router.navigate(["/"]);
  }
  
  isLoggined(){
    return localStorage.getItem("token");
  }

  getName(){
    return localStorage.getItem("fullName");
  }
  getId(){
    return localStorage.getItem("currentUser");
  }

}

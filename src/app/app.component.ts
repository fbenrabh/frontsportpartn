import { Component } from '@angular/core';
import firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontSportPartn';

  constructor() {
  var firebaseConfig = {
    
    // apiKey: "AIzaSyAkIRpN0MO3m8WT7INiZWs6NBgfjUljukM",
    // authDomain: "sportpartn.firebaseapp.com",
    // databaseURL: "https://sportpartn-default-rtdb.firebaseio.com",
    // projectId: "sportpartn",
    // storageBucket: "sportpartn.appspot.com",
    // messagingSenderId: "761527696850",
    // appId: "1:761527696850:web:f05604aff2a059938e9c0b",
    // measurementId: "G-Q42ENS4DNS"

    apiKey: "AIzaSyAZQmsuP71psG4rQjcVDVZiA7OKRslLbz8",
    authDomain: "sportnow-3b801.firebaseapp.com",
    projectId: "sportnow-3b801",
    storageBucket: "sportnow-3b801.appspot.com",
    messagingSenderId: "664350176455",
    appId: "1:664350176455:web:534c727519af625633bae2"


  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
  }

}

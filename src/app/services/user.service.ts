import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  getUser(id:string) :User{
    var user = new User();
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var token = localStorage.getItem("token");
    if(token!==null)
    myHeaders.append("Authorization","Bearer ".concat(token));
  var raw ="";
var requestOptions = {
  method: 'GET',
  headers: myHeaders
};

fetch("https://sportnow-api.herokuapp.com/api/user/"+id, requestOptions)
  .then(response => response.json())
  .then(result => { console.log(result);
     
     user.id = result.id;
     user.name = result.name;
     user.email = result.email;
     user.photo = result.imgUrl;
     user.aboutMe = result.aboutMe;
     user.mySports = result.mySports;

  })
  .catch(error => console.log('error', error));
    return user;
  }


updateUser(id:string,fullName:string,mySports:string,aboutMe:string,img:string) : boolean{

 console.log("img "+img);
 console.log("myspr "+mySports);
 console.log("about "+aboutMe);
 console.log("name "+fullName);


  var myHeaders = new Headers();
myHeaders.append("Content-Type", "application/json");
var token = localStorage.getItem("token");
if(token!==null)
myHeaders.append("Authorization","Bearer ".concat(token));

var raw = JSON.stringify({
  "aboutMe": aboutMe,
  "name": fullName,
  "imgUrl": img,
  "mySports":String(mySports)
});

var requestOptions = {
  method: 'PUT',
  headers: myHeaders,
  body: raw
};

var updated = true;
fetch("https://sportnow-api.herokuapp.com/api/update/user/"+id, requestOptions)
  .then(response =>{ 
   if( response.status !== 200)
   updated = false;
  })
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
  return updated;

}



}

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import firebase from 'firebase';
import { Observable } from 'rxjs';



@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private router: Router) { }


canActivate(route: ActivatedRouteSnapshot):boolean {

const isSignedIn = localStorage.getItem("token");

if (isSignedIn === null) {
     this.router.navigate(['/auth/signin']);
}

return  true;

}

  
 /* canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise(
      (resolve, reject) => {
        firebase.auth().onAuthStateChanged(
          (user) => {
            if(user) {
              resolve(true);
            } else {
              this.router.navigate(['/auth', 'signin']);
              resolve(false);
            }
          }
        );
      }
    );
  }
  */
}
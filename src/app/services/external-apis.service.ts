import { Injectable } from '@angular/core';
import {  SafeResourceUrl,DomSanitizer } from '@angular/platform-browser';
import { GeoEncode } from '../models/geo-encode.model';

@Injectable({
  providedIn: 'root'
})
export class ExternalApisService {

  constructor(private sanitizer :DomSanitizer) { }

  addressGeocoding(address:string,city:string,postalcode:string):GeoEncode {

    var requestOptions = {
      method: 'GET'
    };
    var arr = [] as string[];
    let geoEncode = new GeoEncode();
    geoEncode.bbox = [];
    console.log("adrs: "+address);
    fetch("https://nominatim.openstreetmap.org/search?street="+address+"&city="+city+"&postalcode="+postalcode+"&format=json&polygon=1&addressdetails=1", requestOptions)
      .then(response => response.json())
      .then(result => {console.log(result);
      if(result.length>0){
        geoEncode.lat = result[0].lat;
        geoEncode.lon = result[0].lon;
        for(let i =0;i<4;i++)
          geoEncode.bbox[i] = result[0].boundingbox[i];
          geoEncode.url =this.getMapUrl(geoEncode);
      }
      })
      .catch(error => console.log('error', error));
     return geoEncode;
  }


  getMapUrl(geoEncode:GeoEncode) :string{
    //let geoEncode = this.addressGeocoding(address,city,postalcode);
    console.log("my... "+JSON.stringify(geoEncode));
    let url = "https://www.openstreetmap.org/export/embed.html?bbox="+geoEncode.bbox[2]+"%2C"+geoEncode.bbox[0]+"%2C"+geoEncode.bbox[3]+"%2C"+geoEncode.bbox[1]+"&layer=mapnik&marker="+geoEncode.lat+"%2C"+geoEncode.lon;
    //let url ="https://www.openstreetmap.org/export/embed.html?bbox=1.8518856167793276%2C50.94634874487112%2C1.854396164417267%2C50.94752829403982&amp;layer=mapnik&amp;marker=50.946938523196806%2C1.8531408905982971" ;
    console.log("URLL"+url);
    return url;
    //console.log("safe URLL   "+this.safeUrl);
    //return this.safeUrl;
  }


}

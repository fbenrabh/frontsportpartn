import { Injectable } from '@angular/core';

import firebase from 'firebase/app';
import  auth from 'firebase/app';
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    public afAuth: AngularFireAuth,private router : Router 
  ) { }


  createNewUser(email: string, password: string,fullname:string){
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    
    var raw = JSON.stringify({
      "name":fullname,
      "email": email,
      "password": password
    });
    
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: undefined
    };
    
    fetch("https://sportnow-api.herokuapp.com/api/addUser", requestOptions)
      .then(response => response.text())
      .then(result => {console.log(result);
        this.router.navigate(['/auth/signin']);
      })
      .catch(error => {console.log('error', error);
    });

}

signInUser(email: string, password: string){
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");
  
  var raw = JSON.stringify({
    "email": email,
    "password": password
  });
  
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: undefined
  };

  var auth = false;
  fetch("https://sportnow-api.herokuapp.com/auth", requestOptions)
    .then(response => {
      if(response.status ===200)
      auth = true;
      return response.json();
    })
    .then(result => { 
     
      //let token = result.substr(10).slice(0,-2);
      if(auth){
      localStorage.setItem("token",result.token);
      localStorage.setItem("currentUser",result.idCurrentUser);
      localStorage.setItem("fullName",result.fullName);
     // console.log(localStorage.getItem("fullName"));
      this.router.navigate(['/']);
      }
      console.log("email ou mot de passe incorrect  !");
    })
    .catch(error =>{ console.log('error', error);
  });

}

signOutUser() {
  localStorage.removeItem("token");
  localStorage.removeItem("currentUser");
  localStorage.removeItem("fullName");
}


 // Sign in with Google
 GoogleAuth() {
  return this.AuthLogin(new firebase.auth.GoogleAuthProvider());
}  




// Auth logic to run auth providers
AuthLogin(provider: any) {
  return this.afAuth.signInWithPopup(provider)
  .then((result: any) => {
      console.log('You have been successfully logged in!')
  }).catch((error: any) => {
      console.log(error)
  })
}



}

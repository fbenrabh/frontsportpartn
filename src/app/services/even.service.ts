import { ThrowStmt } from '@angular/compiler';
import { Injectable } from '@angular/core';
import  firebase from 'firebase';
import { Subject } from 'rxjs';

import { Even } from '../models/even.model';
import { GeoEncode } from '../models/geo-encode.model';
import { User } from '../models/user.model';
import { ExternalApisService } from './external-apis.service';
import DataSnapshot = firebase.database.DataSnapshot;

@Injectable({
  providedIn: 'root'
})
export class EvenService {


  eventsList: Even[] = [];
  evens: Even[] = [];
  evensSubject = new Subject<Even[]>();

  
  constructor(private extApiService : ExternalApisService) { 
    this.getEvens();
    
  }


  emitEvens() {
    this.evensSubject.next(this.evens);
  }
  saveEvens() {
    firebase.database().ref('/evens').set(this.evens);
}

getEvens() {
  firebase.database().ref('/evens')
    .on('value', (data: DataSnapshot) => {
        this.evens = data.val() ? data.val() : [];
        this.emitEvens();
      }
    );
}

getSingleEven(id: number) {
  return new Promise <any>(
    (resolve, reject) => {
      firebase.database().ref('/evens/'+id).once('value').then(
        (data: DataSnapshot) => {
          resolve(data.val());
        }, (error) => {
          reject(error);
        }
      );
    }
  );
}

//************** */


createNewEven(even: Even) :boolean{

  console.log(even);
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");
  
  //var event:Even;
  var token = localStorage.getItem("token");
  if(token!==null)
  myHeaders.append("Authorization","Bearer ".concat(token));

  var raw = JSON.stringify({
    "name": even.title,
    "eventDate":even.eventDate,
    "description":even.description,
    "eventAddress":even.eventAddress,
    "city":even.city,
    "postalCode":even.postalCode,
    "sport":even.sport
  });


  
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: undefined
  };
  var added = true;
  fetch("https://sportnow-api.herokuapp.com/api/add/event", requestOptions)
    .then(response => {
      if(response.status !== 200)
      added = false;
    })
    .then(result => console.log(result))
    .catch(error => {console.log('error', error);
  });
  this.eventsList =[];
  //this.getEvents();
  return added;
}

//******************* */
updateEven(id: number,newEven: Even) {
 
  //this.evens.splice(id, 1,newEven);
 // this.saveEvens();
 // this.emitEvens();

}

//************************* */

getEvents(): Even[]{
  this.eventsList = [];
  var empty :Even[]=[];
  var myHeaders = new Headers();
 var token = localStorage.getItem("token");
 if(token==null) 
 return empty;
myHeaders.append("Authorization","Bearer ".concat(token));

var raw = "";

var requestOptions = {
method: 'GET',
headers: myHeaders,
redirect: undefined
};

fetch("https://sportnow-api.herokuapp.com/api/events", requestOptions)
.then(response => response.json())
.then(result => { //console.log(result);


for(let i=0;i<result.length;i++){

var event = new Even(); 
  event.id=result[i].id;
  event.title=result[i].name;
  event.creationDate=result[i].creationDate;
  event.eventDate=result[i].eventDate;
  event.description=result[i].description;
  event.eventAddress=result[i].eventAddress;
  event.city=result[i].city;
  event.sport=result[i].sport;

  if(result[i].author !==null){
    let u =new User();
    u.name = result[i].author.name;
    u.id = result[i].author.id;
    event.author =u; 
    console.log(JSON.stringify(event))
  }
  this.eventsList.push(event);


}


 //console.log(result[0].id);
 //console.log(result[0].name);
})
.catch(error => console.log('error', error));
 return this.eventsList;

}


//******************** */


getEvent(id:string):Even{

  var event:Even;
  event = new Even();
var myHeaders = new Headers();
var token = localStorage.getItem("token");
//if(token==null) 
//return event;
if(token!==null)
myHeaders.append("Authorization","Bearer ".concat(token));

var raw = "";

var requestOptions = {
method: 'GET',
headers: myHeaders,
redirect: undefined
};

fetch("https://sportnow-api.herokuapp.com/api/event/"+id, requestOptions)
.then(response => response.json())
.then(result => { //console.log(result);

 
 event.id=result.id;

 event.title=result.name;
 event.creationDate=result.creationDate;
 event.eventDate=result.eventDate;
 event.description=result.description;
 event.eventAddress=result.eventAddress;
 event.city=result.city;
 event.postalCode=result.postalCode;
 event.sport = result.sport;



   let us =new User();
   us.name = result.author.name;
   us.id = result.author.id;
   us.photo = result.author.imgUrl;
   event.author =us; 
   //console.log(JSON.stringify(event))


   event.participants = [];
  
   for(let i=0;i<result.participants.length;i++){
    let u = new User();
    u.name = result.participants[i].name;
    u.id = result.participants[i].id;
    event.participants.push(u);
   }

   event.geoEncode = new GeoEncode();
   event.geoEncode = this.extApiService.addressGeocoding(event.eventAddress,event.city,event.postalCode);
   console.log("geo "+event.geoEncode);   
   let currentUser = localStorage.getItem("currentUser"); 
   event.isParticipate = false;
   if(event.author.id===currentUser )
   event.isParticipate = true;

   event.participants.forEach(p=> {
     if(p.id === currentUser){
     event.isParticipate = true;
     }
  });

 
//return event;
//console.log(result[0].id);
//console.log(result[0].name);
})
.catch(error => console.log('error', error));

return event;
}

//******************** */

deleteEvent(event: Even) :boolean{
  var id =event.id;
  //console.log("my id: "+id);
  var myHeaders = new Headers();
  var token = localStorage.getItem("token");
if(token!==null)
myHeaders.append("Authorization","Bearer ".concat(token));
myHeaders.append("Content-Type", "application/json");



var requestOptions = {
  method: 'DELETE',
  headers: myHeaders,
};

var deleted = true;
fetch("https://sportnow-api.herokuapp.com/api/delete/event/"+id, requestOptions)
  .then(response => {
    if(response.status !== 200)
    deleted = false;
    else{
      response.json().then(ev =>{
         for(let i=0;i<this.eventsList.length;i++){
           if(ev.id === this.eventsList[i].id){
            this.eventsList.splice(i,1);
            //break;
           }
             
         }
      });
    }
    
  })
  .then(result => console.log(result))
  .catch(error => {console.log('error', error); 
});
this.eventsList =[];
//this.getEvents();
return deleted;

}

//************************ */

participeToEvent(event:Even){
  
  if(event.isParticipate)
  return;
  event.isParticipate=true;
  var myHeaders = new Headers();
  var token = localStorage.getItem("token");
if(token!==null)
myHeaders.append("Authorization","Bearer ".concat(token));
myHeaders.append("Content-Type", "application/json");


var raw = ""

var requestOptions = {
  method: 'POST',
  headers: myHeaders,
  body: raw
};

let currentUser = localStorage.getItem("currentUser");
fetch("https://sportnow-api.herokuapp.com/api/event/participate/"+currentUser+"/"+event.id, requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));

}

//****************** */

removeParticipation(event:Even){
  
  if(!event.isParticipate)
  return;
  

  var myHeaders = new Headers();
  var token = localStorage.getItem("token");
if(token!==null)
myHeaders.append("Authorization","Bearer ".concat(token));
myHeaders.append("Content-Type", "application/json");


var raw = ""

var requestOptions = {
  method: 'POST',
  headers: myHeaders,
  body: raw
};

let currentUser = localStorage.getItem("currentUser");
fetch("https://sportnow-api.herokuapp.com/api/event/removeParticipation/"+currentUser+"/"+event.id, requestOptions)
  .then(response => response.text())
  .then(result => {console.log(result);
    event.isParticipate=false;
  })
  .catch(error => console.log('error', error));

}


/******************* */
getEventsByParticipant(uid:string) : Even[]{

  var myHeaders = new Headers();
  var token = localStorage.getItem("token");
  if(token!==null)
  myHeaders.append("Authorization","Bearer ".concat(token));
  myHeaders.append("Content-Type", "application/json");
  
  var raw = JSON.stringify({});
  
  var requestOptions = {
    method: 'GET',
    headers: myHeaders
  };
  var events :Even[]=[];
  fetch("https://sportnow-api.herokuapp.com/api/events/byParticipant/"+uid, requestOptions)
    .then(response => response.json())
    .then(result => { //console.log(result);
      for(let i=0;i<result.length;i++){
        let event = new Even();
        event.id=result[i].id;
        event.title=result[i].name;
        event.eventDate=result[i].eventDate;
        event.description=result[i].description;
        event.city=result[i].city;
        events.push(event);

      }

    })
    .catch(error => console.log('error', error));
    return events;

}


/***************************** */

getSportsList() : string[]{

  var myHeaders = new Headers();
  var token = localStorage.getItem("token");
  if(token!==null)
  myHeaders.append("Authorization","Bearer ".concat(token));
  myHeaders.append("Content-Type", "application/json");
  
  var raw = JSON.stringify({});
  
  var requestOptions = {
    method: 'GET',
    headers: myHeaders
  };
  var sports :string[]=[];
  fetch("https://sportnow-api.herokuapp.com/api/sports", requestOptions)
    .then(response => response.json())
    .then(result => { 
      console.log(result);
      for(let i=0;i<result.length;i++){
 
        sports.push(result[i]);

      }

    })
    .catch(error => console.log('error', error));
    return sports;

}



//storage
uploadFile(file: File) {
  return new Promise <string>(
    (resolve, reject) => {
      const almostUniqueFileName = Date.now().toString();
      const upload = firebase.storage().ref()
        .child('images/' + almostUniqueFileName + file.name).put(file);
      upload.on(firebase.storage.TaskEvent.STATE_CHANGED,
        () => {
          console.log('Chargement…');
        },
        (error) => {
          console.log('Erreur de chargement ! : ' + error);
          reject();
        },
        () => {
          resolve(upload.snapshot.ref.getDownloadURL());
        }
      );
    }
  );
}





}

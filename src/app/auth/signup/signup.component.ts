import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {


  signupForm!: FormGroup;
  errorMessage!: string;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]],
      fullname:['',[Validators.required]],
      passwordconfirm:['',[Validators.required]]
    });
  }

  onSubmit() {
    // @ts-ignore: Object is possibly 'null'.
    const email = this.signupForm.get('email').value;
    // @ts-ignore: Object is possibly 'null'.
    const password = this.signupForm.get('password').value;
    // @ts-ignore: Object is possibly 'null'.
    const passwordconfirm = this.signupForm.get('passwordconfirm').value;
    // @ts-ignore: Object is possibly 'null'.
    const fullname =  this.signupForm.get('fullname').value;
    if(password !== passwordconfirm)
            return;


    this.authService.createNewUser(email, password,fullname);
    this.router.navigate(['/auth/signin']);
       
  }
 
 
}

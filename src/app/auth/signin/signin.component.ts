import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { MatCarousel, MatCarouselComponent } from '@ngmodule/material-carousel';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class SigninComponent implements OnInit {

  

  signinForm!: FormGroup;
  errorMessage!: string;

  constructor(private formBuilder: FormBuilder,
              public authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.signinForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
    });
  }

  onSubmit() {
    // @ts-ignore: Object is possibly 'null'.
    const email = this.signinForm.get('email').value;
    // @ts-ignore: Object is possibly 'null'.
    const password = this.signinForm.get('password').value;
    
    this.authService.signInUser(email, password);
   
      //this.router.navigate(['/even']);
 
       
  }
  onSubmit2() {
  
    
    this.authService.GoogleAuth().then(
      () => {
        this.router.navigate(['/even']);
      },
      (error) => {
        this.errorMessage = error;
      }
    );
  }
}

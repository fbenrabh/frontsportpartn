import { Component, OnChanges, OnInit } from '@angular/core';
import { EvenService } from '../../services/even.service';
import { UserService } from '../../services/user.service';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import { User } from '../../models/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss']
})
export class ProfileFormComponent implements OnInit  {

  id!:any;
  nameInput!:string;
  aboutmeInput!:string;
  profileForm!: FormGroup;
  img!:any;
  user!:User;




  sports!:string[];

  constructor(private formBuilder: FormBuilder,private eventService  : EvenService, private userService : UserService,private router: Router ) { 

  }

  ngOnInit(): void {
      this.sports = this.eventService.getSportsList();
      if(localStorage.getItem("currentUser") !== null)
      this.id = localStorage.getItem("currentUser");
  
    this.user = this.userService.getUser(this.id);
    console.log("user"+this.user);
      let user = this.userService.getUser(this.id);
     // this.nameInput = user.name;
      //this.aboutmeInput = user.aboutMe;
      this.initForm();
 
    
  }

  initForm() {

  
    this.profileForm = this.formBuilder.group({
      fullName: [String(this.user.name),Validators.required],
      mySports: ['', Validators.required],
      aboutMe: ['', Validators.required],
      profileImg: ['', Validators.required],
      fileSource: ['', Validators.required]

    });
  }

 


  get f(){
    return this.profileForm.controls;
  }
     
  onFileChange(event:Event) {
    const target= event.target as HTMLInputElement;
    const file: File = (target.files as FileList)[0];
    if (file.size > 0) {

      this.profileForm.patchValue({
        fileSource: file
      });
    }
  }
     
  onSaveProfile(){

    let user = new User();
    const fullName = this.profileForm.get('fullName')?.value;
    const mySports = this.profileForm.get('mySports')?.value;
    const aboutMe = this.profileForm.get('aboutMe')?.value;
  

   user.name = fullName;
  // user.photo = url;
   user.aboutMe = aboutMe;
   user.mySports = mySports;
   console.log("fullname  "+mySports);
    const formData = new FormData();
    formData.append('file', this.profileForm.get('fileSource')?.value);
    var img="";
    this.eventService.uploadFile(this.profileForm.get('fileSource')?.value).then(
      (url: string ) => {
      // console.log("url "+url);
        user.mySports = "";
        this.img = url;
        //console.log("user: "+user);

        if(this.userService.updateUser(this.id,fullName,mySports,aboutMe,this.img)){
          let r = '/profile/'+this.id;
          this.router.navigate([r]);
        }
      }
    );
   
   

   
    
  }

}



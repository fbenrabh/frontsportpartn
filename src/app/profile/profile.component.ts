import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import { EvenService } from '../services/even.service';
import { Even } from '../models/even.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss', "../../assets/vendors/bootstrap/css/bootstrap.css",
  "../../assets/vendors/font-awesome/css/fontawesome-all.min.css","../../assets/vendors/magnific-popup/magnific-popup.css",
  "../../assets/css/styles.css"]
})
export class ProfileComponent implements OnInit {
  
  user!:User;
  events!:Even[];

  constructor(private userService:UserService ,private eventService :EvenService, private route:ActivatedRoute) { }

  ngOnInit(): void {
   let id = this.route.snapshot.params['id'];
   this.user = this.userService.getUser(id);
   this.events = this.eventService.getEventsByParticipant(id);
  }

}

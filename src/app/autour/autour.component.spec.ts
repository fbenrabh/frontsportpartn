import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutourComponent } from './autour.component';

describe('AutourComponent', () => {
  let component: AutourComponent;
  let fixture: ComponentFixture<AutourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutourComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

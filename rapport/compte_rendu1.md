# Compte rendu du Mardi 20/04/2021

## Matin

### Daily Meeting

Première réunion du projet, on discute le but géneral , qui consiste à développer un nouveaux service qui permet  aux utilisateurs de touver des partenaires pour pratiquer leurs sport preferé en créant des évenements sportives dont les autres utilisateurs peuvent participer  .

### Sprint planning

Nom | Prénom | Tâche à réaliser
--- | --- | ---
**Benrabh** | Farouk | Préparation de l'environnement de travail pour le front-end et déploiement Gitlab CI+ creer les routes l'application
**Abdellah** | Sami | Préparation de l'environnement de travail pour le back-end et déploiement Gitlab CI


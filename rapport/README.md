# Liste des comptes rendus

Ce document recense tous les comptes rendus effectués durant le projet (sprint planning, daily meeting, sprint review, sprint retrospective). Ils sont séparés en fonction du jour auquel ils ont été réalisés.

* [Compte rendu du Mardi 20/04/2021](./compte_rendu1.md)

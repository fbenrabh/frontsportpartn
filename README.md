# FrontSportPartn

L'équipe **SportPartn** est composée des membres suivants :
* Farouk Benrabh
* Sami Abdellah

## But de projet

Ce projet permet  aux utilisateurs de savoir la meteo dans leurs ville et touver des partenaires pour pratiquer leurs sport preferé en créant des évenements sportives dont les autres utilisateurs peuvent participer .

## Api Utilisés

* [Open Weater map pour savoir la meteo](https://openweathermap.org)
* [Open street map pour afficher l'adresse de l'evenement dans la map](https://www.openstreetmap.org/#map=6/46.449/2.210)
* [Notre propre api réaliser avec SpringBoot pour stocker les utilisateurs et les evenements](https://gitlab.com/fbenrabh/backsportpartn)
* Api réaliser avec firebase pour stocker les images des utilisateurs

## Version en ligne

Le projet **SportPartn** est déployé en ligne sur Herokuapp. Ce qui permet de tester que nos modifications fonctionnent bien en ligne.
* [Lien de la version de développement](https://blooming-reef-00698.herokuapp.com//)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).


